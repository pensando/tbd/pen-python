#!/usr/bin/env python
# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ryan Tischer ryan@pensando.io
#            Edward Arcuri edward@pensando.io
#
#
import os
import sys
import gzip
import logging

from io import BytesIO
from time import sleep
from pprint import pprint as pp
from datetime import datetime, timedelta

# This allows us to import the PSM class from the lib directory just for these examples
libpath = os.path.dirname(os.path.abspath(__file__))
sys.path[:0] = [os.path.join(libpath, os.pardir, 'lib')]
#from utils import returnTime
from pensando import logger
from pensando.psmapi import PSM
from pensando.psm_modules.utils import *


if __name__ == "__main__":

    # Set up logging for this app.  Pensando library defaults to debug for everything
    # homeDir = os.path.expanduser('~')
    # loggerDict = PenConfig('LOGGING',f"{homeDir}/.penrc")
    # if 'level' in loggerDict.getKeys():
    #     logLevel = getattr(logging,loggerDict.getValue('level'))
    #     logger.setLevel(logLevel)

    logger.setLevel(logging.DEBUG)
    # Instantiate a PSM object
    #myPSM = PSM()
    myPSM = PSM(server="https://10.29.82.46",user='admin',password="Pensando0$",tenant='default')

    # EXAMPLE getMirrorSessions()
    # Get information about currently configured Mirror Sessions  (ERSPAN)
    try:
        info = myPSM.getMirrorSessions()
        print("\n\nCurrently Configured ERSPAN sessions")
        print("="*50)
        pp(info)      #Uncomment to print out all cluster info in JSON
    except Exception as e:
        logger.error(f"Unable to retrieve ERSPAN info: {e}")
    # END getMirrorSessions()


#     '''
#     {
#   "kind": "string",
#   "api-version": "string",
#   "meta": {
#     "name": "string",
#     "tenant": "string",
#     "namespace": "string",
#     "generation-id": "string",
#     "resource-version": "string",
#     "uuid": "string",
#     "labels": "object",
#     "creation-time": "string (date-time)",
#     "mod-time": "string (date-time)",
#     "self-link": "string"
#   },
#   "spec": {
#     "packet-size": "integer (int64)",
#     "start-condition": {
#       "schedule-time": "string (date-time)"
#     },
#     "collectors": [
#       {
#         "type": "string",
#         "export-config": {
#           "destination": "10.1.1.1 ",
#           "gateway": "string",
#           "virtual-router": "string"
#         },
#         "strip-vlan-hdr": "boolean (boolean)"
#       }
#     ],
#     "match-rules": [
#       {
#         "source": {
#           "ip-addresses": [
#             "string"
#           ],
#           "mac-addresses": "aabb.ccdd.0000, aabb.ccdd.0000, aabb.ccdd.0000"
#         },
#         "destination": {
#           "ip-addresses": [
#             "string"
#           ],
#           "mac-addresses": "aabb.ccdd.0000, aabb.ccdd.0000, aabb.ccdd.0000"
#         },
#         "app-protocol-selectors": {
#           "proto-ports": "udp/1234",
#           "applications": [
#             "string"
#           ]
#         }
#       }
#     ],
#     "packet-filters": [
#       "string"
#     ],
#     "interfaces": {
#       "direction": "string",
#       "selectors": [
#         {
#           "requirements": [
#             {
#               "key": "string",
#               "operator": "string",
#               "values": [
#                 "string"
#               ]
#             }
#           ]
#         }
#       ]
#     },
#     "span-id": "integer (int64)",
#     "workloads": {
#       "direction": "string",
#       "selectors": [
#         {
#           "requirements": [
#             {
#               "key": "string",
#               "operator": "string",
#               "values": [
#                 "string"
#               ]
#             }
#           ]
#         }
#       ]
#     },
#     "source": {
#       "target-type": "string",
#       "direction": "string",
#       "target-selectors": [
#         {
#           "requirements": [
#             {
#               "key": "string",
#               "operator": "string",
#               "values": [
#                 "string"
#               ]
#             }
#           ]
#         }
#       ]
#     },
#     "disabled": "boolean (boolean)"
#   }
# }'''
