import os
import sys
import json
import logging

from pprint import pprint

# This allows us to import the PSM class from the lib directory just for these examples
libpath = os.path.dirname(os.path.abspath(__file__))
sys.path[:0] = [os.path.join(libpath, os.pardir, 'lib')]
from pensando import logger
from pensando.psmapi import PSM, PenConfig

if __name__ == "__main__":
    myPSM = PSM()
    # EXAMPLE getFWLogPolicies()
    try:
        response = myPSM.getFWLogPolicies()
        print(response)
    except Exception as e:
        print(f"{e}")
    # END getFWLogPolicies()

    # EXAMPLE getAlerts()
    #  Get all alerts that are labeled as "CRITICAL"
    try:
        alerts = myPSM.getAlerts()
        for alert in alerts['items']:
            if alert['status']['severity'] == 'critical':
                pprint(alert)
    except Exception as e:
        print(f"{e}")
    # END getAlerts()
