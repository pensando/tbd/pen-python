
import json
from logging import NullHandler
from pprint import pprint as pp

workloadName = "tester"
hostName = "testHost"
ipAddress = "1.2.3.4"
macAddress = '1111.2322.3212'
usegVLAN = '900'
extVLAN = '901'
requestDict = {'meta':{},'spec':{}}

intfList = ()
specInterfaces = [dict()]
specInterfaces[0]['mac-address'] = f'{macAddress}'
specInterfaces[0]['micro-seg-vlan'] = f'{usegVLAN}'
specInterfaces[0]['external-vlan'] = f'{extVLAN}'
# specInterfaces[0]['network'] = 'null'

if ipAddress != None:
    specInterfaces[0]['ip-addresses'] = [f"{{{ipAddress}}}"]


print(f"Interfaces should be: {specInterfaces}")


# if ipAddress is None:
#     specInterfaces = {['mac-address': f"'{macAddress}'",'micro-seg-vlan': f"'{usegVLAN}'", 'external-vlan': f"'{extVLAN}'",'network': null]}
# else:
#     specInterfaces = (f'[{{"mac-address": "{macAddress}","micro-seg-vlan": "{usegVLAN}"",'
#                         f'"external-vlan": {extVLAN},"ip-addresses": [ "{ipAddress}" ],'
#                         f'"network": null]}}')

requestDict['meta']['name'] = workloadName
requestDict['spec']['host-name'] = hostName
requestDict['spec']['interfaces'] = specInterfaces

pp(f"{json.dumps(requestDict)}")
