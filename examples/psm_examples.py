#!/usr/bin/env python
# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Ryan Tischer ryan@pensando.io
#            Edward Arcuri edward@pensando.io
#
#
import os
import sys
import logging

# This allows us to import the PSM class from the lib directory just for these examples
libpath = os.path.dirname(os.path.abspath(__file__))
sys.path[:0] = [os.path.join(libpath, os.pardir, 'lib')]
from pensando import logger
from pensando.psmapi import PSM, PenConfig
from pprint import pprint as pp
from pensando.psm_modules.utils import *


if __name__ == "__main__":

    # Set up logging for this app.  Pensando library defaults to debug for everything
    homeDir = os.path.expanduser('~')
    # loggerDict = PenConfig('LOGGING',f"{homeDir}/.penrc")
    # if 'level' in loggerDict.getKeys():
    #     logLevel = getattr(logging,loggerDict.getValue('level'))
    #     logger.setLevel(logLevel)

    logger.setLevel(logging.DEBUG)
    # Instantiate a PSM object
    #myPSM = PSM()
    myPSM = PSM(server="https://10.10.100.212",user='admin',password="Pensando0$",tenant='default')

    # EXAMPLE getClusterInfo()
    # Get information about the PSM cluster
    # try:
    #     info = myPSM.getClusterInfo()
    #     print("\n\nCLUSTER INFO")
    #     print("="*50)
    #     for item in info['meta']:
    #             pp(f"{item}: {info['meta'][item]}")
    #     for item in info['spec']:
    #             pp(f"{item}: {info['spec'][item]}")
    #     for item in info['status']:
    #             pp(f"{item}: {info['status'][item]}")
    #     pp(info)      #Uncomment to print out all cluster info in JSON
    # except Exception as e:
    #     logger.error(f"Unable to retrieve cluster info: {e}")
    # END getClusterInfo()


    # # EXAMPLE getWorkloads()
    # # Get all workloads known by your PSM and display the name
    try:
        workLoads = myPSM.getWorkloads()
        print("\n\nWORKLOADS")
        print("="*50)
        for workLoad in workLoads['items']:
            print(f"Workload Name: {workLoad['meta']['name']}")
        pp(workLoads)   # Uncomment to print out json of all workloads
    except Exception as e:
        logger.error(f"Unable to retrieve workload info: {e}")


    # EXAMPLE getDSCs()
    # Get all DSC info and print out the mac and the DSCVersion
    # try:
    #     dscData = myPSM.getDSCs()
    #     # dscList = dscData['items']
    #     # print("\n\nDSCs")
    #     # print("="*50)
    #     # for dsc in dscList:
    #     #     print(f"Name: {dsc['spec']['id']}, Mac: {dsc['meta']['name']}, Ver: {dsc['status']['DSCVersion']}")
    #     pp(dscData)    # Uncomment to print out json of all DSCs

    # except Exception as e:
    #     logger.error(f"Unable to retrieve info about DSCs: {e}")

    # EXAMPLE getDSCMacs()
    # Get all DSC mac addys and list 'em out
    # try:
    #     dscList = myPSM.getDSCMacs()
    #     print("\n\nDSC MACs")
    #     print("="*50)
    #     for mac in dscList: print(mac)
    #     sleep(2)
    # except Exception as e:
    #     logger.error(f"Unable to retrieve DSC mac addresses: {e}")

    # # EXAMPLE getUsers()
    # # Get all users from PSM.  Then use the userPrefs() method to get their prefs
    # # by sending it the username
    # try:
    #     users = myPSM.getUsers()
    #     print("\n\nUSERS & THEIR PREFERENCES")
    #     print("="*50)
    #     for user in users['items']:
    #         print(f"\n\nUser: {user['meta']['name']}    Email:  {user['spec']['email']}")
    #         userPrefs = myPSM.getUserPrefs(user['meta']['name'])
    #         print("-"*20)
    #         for item in userPrefs['meta']:
    #             print(f"{item}:{userPrefs['meta'][item]}")
    #     # pp(users)      # Uncomment to print out all json of all Users
    # except Exception as e:
    #     logger.error(f"Unable to retrieve user info: {e}")
    # # END getUsers()

    # # EXAMPLE getNetworkSecurityPolicies()
    # # Get all security policies defined on the cluster
    # try:
    #     policies = myPSM.getNetworkSecurityPolicies()
    #     print("\n\nNETWORK SECURITY POLICIES")

    #     for item in policies['items']:
    #         print(f"Security policy {item['meta']['name']} has {len(item['spec']['rules'])} rules defined")
    #     # pp(policies)     # Uncomment to print out json of all security policies
    # except Exception as e:
    #     logger.error(f"Unable to retrieve Network Security Policy info: {e}")
    # # END getNetworkSecurityPolicies()

    # # EXAMPLE addWorkloadLabel()
    # # Create a new label for a workload
    # labelKey = "test.label3"
    # labelValue = "label3_value"
    # workloadName = "orch1--vm-90"

    # print(f"\n\nADDING WORKLOAD LABEL {labelKey}: {labelValue}")
    # print("="*50)
    # try:
    #     pprint(myPSM.addWorkloadLabel(workloadName, labelKey, labelValue))
    # except Exception as e:
    #     logger.error(f"Unable to add label to workload {workloadName}: {e}")
    # # END addWorkloadLabel()

    # # EXAMPE delWorkloadLabel()
    # labelKey = "test.label"
    # workloadName = "orch1--vm-90"
    # print(f"\n\nDELETING WORKLOAD LABEL {labelKey}")
    # print("="*50)
    # try:
    #     pprint(myPSM.delWorkloadLabel(workloadName, labelKey))
    # except Exception as e:
    #     logger.error(f"Unable to delete label {labelKey} from workload {workloadName}: {e}")
    # # END delWorkloadLabel()

    # # EXAMPLE getFWLogPolicies()
    # example = PSM()
    # # END getFWLogPolicies()
    # # # resp = myPSM.addWorkload("testworkload","v10","1243.4312.abcf","100","999",ip="5.7.4.5")
    # # # print(resp)

    # # myPSM.delWorkload("stilldoesntexist")


    # EXAMPLE getAlerts()
    #  Get all alerts that are labeled as "CRITICAL"

    # alert = myPSM.getAlertObject("3850e076-610b-4bcf-b9e2-d4f1720c9f57")
    # pp(alert)
    # # END getAlerts()

    # # EXAMPLE getEvents()
    # pp(myPSM.getEvents())
    # END getEvents()
    #print(myPSM.getMetrics(mt="MgmtMacMetrics",fl=("FramesRx_1024B_1518B")))
    #print(myPSM.getMetrics(mt='LifMetrics',fl=('RxBroadcastPackets','RxDropBroadcastPackets')))

#     dataStructure = '''
#     {

#         "function": "last",
#         "start-time": "2020-01-23T18:52:57Z",
#         "end-time": "2020-01-23T18:53:57Z",
#         "group-by-time": "60m",
#         "sort-order": "descending"
#     } '''
# response = myPSM.getMetrics(ds=dataStructure)
# print(response)
