import os
import sys
import json
import logging

# This allows us to import the PSM class from the lib directory just for these examples
libpath = os.path.dirname(os.path.abspath(__file__))
sys.path[:0] = [os.path.join(libpath, os.pardir, 'lib')]
from pensando import logger
from pprint import pprint as pp
from pensando.psmapi import PSM, PenConfig



 # Set up logging for this app.  Pensando library defaults to debug for everything
homeDir = os.path.expanduser('~')
loggerDict = PenConfig('LOGGING',f"{homeDir}/.penrc")
if 'level' in loggerDict.getKeys():
    logLevel = getattr(logging,loggerDict.getValue('level'))
    logger.setLevel(logLevel)

# logger.setLevel(logging.DEBUG)

myPSM = PSM(server="https://10.29.82.46",user='admin',password="Pensando0$",tenant='default')

# EXAMPLE getMetrics()
# Get Mgmt interface metrics of Rx frames that are 1KB to 1.5KB in size
# Sending metric type (mt) and the fields list (fl) associated with the
# metric type.
response = myPSM.getMetrics(mt="MgmtMacMetrics",fl=("FramesRx_1024B_1518B"))
print(response)

# # Same as above but also have the results grouped by 30m rather than the default
# # of 60m
response = myPSM.getMetrics(mt="MgmtMacMetrics",fl=("FramesRx_1024B_1518B"),gt="30m")
print(response)

# Defining our own data structure and sending that rather than parameters to build
# the data structure in the method.  Notice we aren't creating the entire data structure
# just the info that is necessary.
dataStructure = '''
    {
        "queries": [
          {
            "kind": "MgmtMacMetrics",
            "fields": ["FramesRx_1024B_1518B"]
          }
        ],
        "function": "last",
        "start-time": "2021-05-29T18:52:57Z",
        "end-time": "2021-06-01T18:53:57Z",
        "group-by-time": "60m",
        "sort-order": "descending"
    } '''
response = myPSM.getMetrics(ds=dataStructure)
print(response)

# In this example we are gathering the the LifMetrics for a particular interface.  Change
# to work with your DSCs as neccessary.
#
dataStructure = '''
{
    "queries": [
      {
        "kind": "LifMetrics",
        "api-version": null,
        "name": null,
        "selector": {
          "requirements": [
            {
              "key": "name",
              "operator": "in",
              "values": [
                "00ae.cd01.0a40-pf-70"
              ]
            }
          ]
        },
        "fields": [
          "RxBroadcastBytes",
          "RxDropBroadcastBytes",
          "RxDropMulticastPackets",
          "RxMulticastBytes",
          "RxPkts",
          "RxUnicastBytes",
          "RxMulticastPackets",
          "RxDropUnicastBytes",
          "RxDropBroadcastPackets",
          "RxBroadcastPackets",
          "RxBytes",
          "RxDropMulticastBytes",
          "RxDropUnicastPackets",
          "RxUnicastPackets"
        ],
        "function": "last",
        "start-time": "2021-05-31T22:06:40.352Z",
        "end-time": "2021-06-01T22:06:40.352Z",
        "group-by-time": "70m",
        "group-by-field": "name",
        "sort-order": "ascending",
        "subquery": null
      }
      ]
      }
    '''

response = myPSM.getMetrics(ds=dataStructure)
pp(response)
# END getMetrics()
