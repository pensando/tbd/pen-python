# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import json
from .base import PSMBase


class MonitoringModule(PSMBase):
    """
    Provides stubs for calling APIs related to monitoring endpoints in PSM
    """
    def getAlerts(self):
        """
        List all alert objects

        :return: List of all alert objects from PSM server
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/alerts"
        return self._get_json(url).json()

    def getAlertObject(self,name):
        """
        Get individual Alert by name.

        :param name: name - derived from meta.name of getAlerts()
        :type name: string
        :return: Info object for named alert
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/alerts/{name}"
        return self._get_json(url).json()

    def getFlowExportPolicy(self):
        """
        Get the Flow Export Policy

        :return: Flow Export Policy info
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/flowExportPolicy"
        return self._get_json(url).json()

    def getFWLogPolicies(self):
        """
        Get all FW log policies - for tenant - from PSM

        :return: Response from server containing list of all FW log policies for tenant
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/fwlogPolicy"
        return self._get_json(url).json()

    def getFWLogPolicy(self,policyName):
        """
        Get the specific FW policy by name

        :param policyName: FW policy name
        :type policyName: string
        :return: The FW log policy
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/fwlogPolicy/{policyName}"
        return self._get_json(url).json()

    def addMirrorSession(self,dataStructure):
        """
        Add a mirror session based on info in dataStructure sent

        :param dataStructure: JSON of settings for ERSPAN session
        :type workloadName: string
        :return: Response from the PSM server attempt to add mirror session
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/MirrorSession"
        return self._post_json(url,json.dumps(dataStructure)).json()

    def delMirrorSession(self,sessionID):
        """
        Delete the specified ERSPAN session from PSM

        :param sessionID: Session ID
        :type sessionID: string
        :return: output of the delete call from server
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/MirrorSession/{sessionID}"
        return self._delete(url).json()

    def getMirrorSessions(self):
        """
        Get the list of currently configured mirror sessions

        :return: list of mirrorsession objects
        :rtype: json
        """
        url = f"{self.server}/configs/monitoring/v1/tenant/{self.tenant}/MirrorSession"
        return self._get_json(url).json()
