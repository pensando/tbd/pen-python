# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import json

from .base import PSMBase


class WorkloadModule(PSMBase):
    """
    Provides stubs for calling APIs related to workloads in PSM
    """

    # TODO: This needs to be fixed and added to the base system.  Right now we
    # get an unmarshaling error from the api when submitting the json structure

    # def addWorkload(self,workloadName,hostName,macAddress,extVLAN,usegVLAN,**kwargs):

    #     url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/workloads"
    #     # TODO: allow for multiple IP addresses this only takes in one.
    #     ipAddress = kwargs.get('ip', None)
    #     network = kwargs.get('network', None)
    #     requestDict = {'meta':{},'spec':{}}

    #     specInterfaces = [dict()]
    #     specInterfaces[0]['mac-address'] = f'{macAddress}'
    #     specInterfaces[0]['micro-seg-vlan'] = f'{usegVLAN}'
    #     specInterfaces[0]['external-vlan'] = f'{extVLAN}'

    #     if ipAddress != None:
    #         specInterfaces[0]['ip-addresses'] = [f"{{{ipAddress}}}"]

    #     requestDict['meta']['name'] = workloadName
    #     requestDict['spec']['host-name'] = hostName
    #     requestDict['spec']['interfaces'] = specInterfaces

    #     print(f"{json.dumps(requestDict)}")

    #     return self._post_json(url,json.dumps(requestDict)).json()

    def addWorkloadLabel(self,workloadName,labelKey,labelValue):
        """
        Add a label to a workload

        :param workloadName: Name of the workload to add the label to
        :type workloadName: string
        :param labelKey: Label name
        :type labelKey: string
        :param labelValue: Label value
        :type labelValue: string
        :return: Response from the PSM server attempt to add label
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/workloads/{workloadName}/label"
        currentLabels = self.getWorkloadLabels(workloadName)
        newLabels = {"labels":{}}

        # Iterate through current labels and add them to new label dict
        for item in currentLabels:
            newLabels['labels'][item] = currentLabels[item]

        # Add the new k/v pair for the label to the end of the new label dict
        newLabels['labels'][labelKey] = labelValue

        return self._post_json(url,json.dumps(newLabels)).json()

    def updateWorkloadLabels(self,workloadName,labels):
            """
            Merge the supplied labels to the existing labels in the workload

            :param workloadName: Name of the workload to add the label to
            :type workloadName: string
            :param labels: Labels to merge
            :type labelKey: json formatted string
            :return: Response from the PSM server attempt to add labels
            :rtype: json
            """
            url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/workloads/{workloadName}/label"
            currentLabels = self.getWorkloadLabels(workloadName)
            newLabels = {"labels":{}}

            # Iterate through current labels and add them to new label dict
            for item in currentLabels:
                newLabels['labels'][item] = currentLabels[item]

            # Add the new k/v pairs for the labels to the end of the new label dict
            for item in labels:
                newLabels['labels'][item] = labels[item]

            return self._post_json(url,json.dumps(newLabels)).json()

    def delWorkload(self,workloadName):
        """
        Remove a workload from PSM

        :param workloadName: Canonical name of the workload
        :type workloadName: string
        :return: output of the delete call from server
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/workloads/{workloadName}"
        return self._delete(url).json()

    def delWorkloadLabel(self,workloadName,labelKey):
        """
        Delete the specified label from the workload

        :param workloadName: Name of the workload to delete the label from
        :type workloadName: string
        :param labelKey: The name of the label to delete
        :type labelKey: string
        :return: New set of labels that are associated with the workload
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/tenant/{self.tenant}/workloads/{workloadName}/label"
        currentLabels = self.getWorkloadLabels(workloadName)
        newLabels = {"labels":{}}

        # Iterate through current labels and add them to new label dict
        for item in currentLabels:
            if item == labelKey:
                pass
            else:
                newLabels['labels'][item] = currentLabels[item]
        return self._post_json(url,json.dumps(newLabels)).json()


    def getWorkload(self,workload):
        """
        Get all information about the specified workload

        :param workload: Canonical name of the workload
        :type workload: string
        :return: All k/v pairs associated with info about workload
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/workloads/{workload}"
        return self._get_json(url).json()

    def getWorkloadLabels(self,workload):
        """
        Get the labels that are currently applied to the workload specified

        :param workload: Canonical name of the workload
        :type workload: string
        :return: All k/v pairs of labels and their values
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/workloads/{workload}"
        workloadInfo = self._get_json(url).json()
        return workloadInfo['meta']['labels']

    def getWorkloads(self):
        """
        Get info dump of all workloads on PSM server

        :return: All information on workloads assigned to tenant
        :rtype: json
        """
        url = f"{self.server}/configs/workload/v1/workloads"
        return self._get_json(url).json()
