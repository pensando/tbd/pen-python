# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
from .base import PSMBase


class ObjstoreModule(PSMBase):
    """
    Provides stubs for calling APIs related to object store endpoints in PSM
    """

    def getFWLogs(self,dscID,startTime,endTime):
        """
        Download CSV file from PSM of DSC firewall logs

        :param dscID: ID (mac) of DSC to get logs for
        :type dscID: string
        :param startTime: Start time of search for logs in the format: |br| "2020-12-21T20:54:25Z"
        :type startTime: string
        :param endTime: End time of search for logs in format: |br| "2020-12-21T20:59:25Z"
        :type endTime: string
        :return: Zipfile containing CSV of all log lines processed between start and end time
        :rtype: Bytes Object
        """
        # Generate the log first
        genURL = (f"{self.server}/objstore/v1/tenant/{self.tenant}/fwlogs/objects?field-selector="
                  f"start-time={startTime},end-time={endTime},dsc-id={dscID},vrf-name={self.tenant}")
        response = self._get_json(genURL)

        # Get download link from the response
        try:
            tmpLink = str(response.json()['items'][0]['meta']['name'])
            newLink = tmpLink.replace("/", "_")
            url = f"{self.server}/objstore/v1/downloads/tenant/default/fwlogs/{newLink}"
            logData = self._get_json(url)
            return logData.content
        except Exception as e:
            print(f"Unable to get logs - {e}")
