import json
import logging
import requests

DEFAULT_TIMEOUT = 30
"""The default timeout for most requests."""


class PSMBase:
    """A base class for API categories: Classes that provide methods which call the
    API to provide some functionality. Included are methods to make
    communicating with the API easier.
    """

    def _delete(self, url, *payload):
        """
        Hidden class method that is the wrapper for all REST 'DELETE' calls
        :param payload: json request and any header info needed for the call
        :type payload: json
        :returns: request object of response from server
        :rtype: requests object
        """

        data = {} if not payload else payload[0]
        logging.debug(f"DELETE payload data is {data}")

        try:
            logging.debug(f"Attempting DELETE call using the url: {url}")
            return self.session.delete(url,timeout=DEFAULT_TIMEOUT,data=json.dumps(data))
        except requests.exceptions.Timeout as to:
            pass
            logging.error(f'Network Timeout: {to}')
        except requests.exceptions.TooManyRedirects as tmr:
            pass
            logging.error(f'Too Many Redirects: {tmr}')
        except requests.exceptions.ConnectionError as ce:
            pass
            logging.error(f'Connection Error: {ce}')
        except requests.exceptions.RequestException as err:
            logging.error(f'Something went wrong but not sure what')
            raise SystemExit(err)

    def _get_json(self, url, *payload):
        """
        Hidden class method that is the wrapper for all REST 'GET' calls
        :param payload: json request and any header info needed for the call
        :type payload: json
        :returns: request object of response from server
        :rtype: requests object
        """

        data = {} if not payload else payload[0]
        logging.debug(f"GET payload data is {data}")

        try:
            logging.debug(f"Attempting GET call using the url: {url}")
            return self.session.get(url,timeout=DEFAULT_TIMEOUT,data=json.dumps(data))
        except requests.exceptions.Timeout as to:
            pass
            logging.error(f'Network Timeout: {to}')
        except requests.exceptions.TooManyRedirects as tmr:
            pass
            logging.error(f'Too Many Redirects: {tmr}')
        except requests.exceptions.ConnectionError as ce:
            pass
            logging.error(f'Connection Error: {ce}')
        except requests.exceptions.RequestException as err:
            logging.error(f'Something went wrong but not sure what')
            raise SystemExit(err)

    def _post_json(self, url, *payload):
        """
        Hidden class method that is the wrapper for all REST 'GET' calls
        :param payload: json request and any header info needed for the call
        :type payload: json
        :returns: request object of response from server
        :rtype: requests object
        """
        data = {} if not payload else payload[0]
        logging.debug(f"POST payload data is {data}")

        try:
            return self.session.post(url=url,timeout=DEFAULT_TIMEOUT,data=data)
        except requests.exceptions.Timeout as to:
            pass
            logging.error(f'Network Timeout: {to}')
        except requests.exceptions.TooManyRedirects as tmr:
            pass
            logging.error(f'Too Many Redirects: {tmr}')
        except requests.exceptions.ConnectionError as ce:
            pass
            logging.error(f'Connection Error: {ce}')
        except requests.exceptions.RequestException as err:
            logging.error(f'Something went wrong but not sure what')
            raise SystemExit(err)
