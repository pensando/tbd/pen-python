# Copyright (c) 2020, Pensando Systems
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# Author(s): Edward Arcuri edward@pensando.io
#
#
import time
import random
import socket

# #from kafka import KafkaProducer, KafkaConsumer
# from kafka.errors import KafkaError
from datetime import datetime, timedelta, timezone


# def send2Kafka(topic,line):
#     kafkaServer = '10.29.75.171:32092'

#     try:
#         producer = KafkaProducer(bootstrap_servers=kafkaServer,retries=5)
#         producer.send(topic,line)
#     except KafkaError as ke:
#         print(f"Error from Kafka: {ke}")
#     except Exception as e:
#         print(f"Error sending line to {topic} on {kafkaServer}: {line}{e}")


# def getFromKafka(topic,next=True):
#     kafkaServer = '10.29.75.171:32092'
#     try:
#         consumer=KafkaConsumer(topic,group_id='pensando',auto_offset_reset='earliest',bootstrap_servers=kafkaServer)
#         value = 0
#         try:
#             for msg in consumer:
#                 print(f"{msg.topic}:{msg.partition}:,{msg.offset}: key={msg.key}, value={msg.value}")
#                 value += 1
#             # print(value)
#         except KeyboardInterrupt:
#             sys.exit()
#     except KafkaError as ke:
#         print(f"Error from Kafka: {ke}")
#     except Exception as e:
#         print(f"Error getting line from {topic} on {kafkaServer}: {e}")


def sendLog(host, port, msg):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)   # UDP
    sock.sendto(bytes(msg, "utf-8"), (host, port))
    sock.close()


def randomLine(afile):
    line = next(afile)
    for num, aline in enumerate(afile):
        if random.randrange(num + 2): continue
        line = aline
    return line


def calcDate(direction,ndays=0,dateFormat="n"):
    if dateFormat == "T":
        if direction == "past":
            return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y-%m-%dT%H:%M:%SZ}"
        elif direction == "now":
            return f"{datetime.now(tz=timezone.utc):%Y-%m-%dT%H:%M:%SZ}"
        else:
            return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y-%m-%dT%H:%M:%SZ}"
    else:
        if direction == "past":
            return f"{datetime.now(tz=timezone.utc) - timedelta(days=ndays):%Y/%m/%d %H:%M:%SZ}"
        elif direction == "now":
            return f"{datetime.now(tz=timezone.utc):%Y/%m/%d T%H:%M:%SZ}"
        else:
            return f"{datetime.now(tz=timezone.utc) + timedelta(days=ndays):%Y/%m/%d %H:%M:%SZ}"


def strTimeProp(start, end, dateFormat, prop):
    """Get a time at a proportion of a range of two formatted times.
    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, dateFormat))
    etime = time.mktime(time.strptime(end, dateFormat))
    ptime = stime + prop * (etime - stime)
    return time.strftime(dateFormat, time.localtime(ptime))


def randomDate(start, end, prop,dateFormat='%Y/%m/%d %H:%M:%S'):
    return strTimeProp(start, end, dateFormat, prop).strip()


def returnTime(now, t5):
    # return_time formats string for use with PSM API start and endtime

    fmonth = now.strftime("%m")
    day = now.strftime("%d")
    hour = now.strftime("%H")
    minute = now.strftime("%M")
    second = now.strftime("%S")
    tDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"
    fmonth = t5.strftime("%m")
    hour = t5.strftime("%H")
    minute = t5.strftime("%M")
    second = t5.strftime("%S")

    tminusDash = f"{now.year}-{fmonth}-{day}T{hour}:{minute}:{second}Z"

    return tDash, tminusDash


def main():
    pass


if __name__ == "__main__":
    main()
