Auth
=========

Auth is used for Authentication and Authorization related APIs.

API Methods
-----------

.. automethod:: pensando.PSM.getAuthenticationPolicy
.. automethod:: pensando.PSM.getRoleBindings
.. automethod:: pensando.PSM.getUser
.. automethod:: pensando.PSM.getUsers
.. automethod:: pensando.PSM.getUserPrefs

Data Structures
---------------

None at this time

Examples
--------

Get all users

.. literalinclude:: ../examples/psm_examples.py
    :language: python
    :start-after: # EXAMPLE getUsers()
    :end-before: # END getUsers()




.. |br| raw:: html

   <br />
