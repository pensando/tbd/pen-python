.. pen-python documentation master file, created by
   sphinx-quickstart on Wed Dec  2 16:09:16 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pensando Python API
===================

.. toctree::
   :maxdepth: 2

   overview
   psm_api_class
   auth
   cluster
   events
   monitoring
   objstore
   security
   telemetry
   workloads
   config_file


.. |br| raw:: html

   <br />


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
