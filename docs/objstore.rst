Object Store
============

Download logs from PSM


API Methods
-----------

.. automethod:: pensando.PSM.getFWLogs


Data Structures
---------------

None at this time

Examples
--------

Get info on all Network Security Policies

.. literalinclude:: ../examples/psm_examples.py
    :language: python
    :start-after: # EXAMPLE getFWLogs()
    :end-before: # END getFWLogs()




.. |br| raw:: html

   <br />
