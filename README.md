## THIS PROJECT IS NO LONGER UNDER ACTIVE DEVELOPMENT
## If you need python support for any version of PSM (cloud, ent or taormina) see the new openapi based libraries on pypi: pensando-dss, pensando-cloud and pensandp-enterprise

# pen-python
pen-python is a Python3 package for use with the Pensando Platform.  It provides
library components for use in Python scripts/applications to interact directly with:

- Pensando Policy Servers Manager (PSM)
- Pensando Distributed Services Card (DSC)  (__still in development__)


## Documentation
Documentation on how to use pen-python can be found [here](https://pensando.gitlab.io/tbd/pen-python/index.html)
